package org.devais.fibonachi;

import java.util.Scanner;

public class Fibonachi {

    /**
     * Задание 2
     * Необходимо написать рекурсивный алгоритм, который находит числа Фибоначчи в
     * пределах от 1 до N. N вводится вручную во время выполнения программы.
     */
    public static void main(String[] args) {
        System.out.println("Enter the number: \n recommend no more than 40");
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] fibSeriesRec = fibByRec(num);
        for (int i = 0; i < fibSeriesRec.length; i++) {
            System.out.print(" " + fibSeriesRec[i] + " ");
        }
    }

    private static int fibRec(int num) {
        if (num == 0) {
            return 0;
        } else if (num == 1 || num == 2) {
            return 1;
        } else {
            return fibRec(num - 1) + fibRec(num - 2);
        }
    }

    private static int[] fibByRec(int num) {
        int[] fib = new int[num];
        for (int i = 0; i < num; i++) {
            fib[i] = fibRec(i);
        }
        return fib;
    }
}
