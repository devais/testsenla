package org.devais.array;

import java.util.*;
import java.util.List;

public class Array {

    /**
     * Задание 3
     * Необходимо написать алгоритм, выполняющий сортировку массива целых чисел по
     * убыванию. Массив вводится вручную во время выполнения программы.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter an array of whole numbers");
        //Create array
        List<Integer> arr = new ArrayList<>();
        String line = null;
        while ((line = sc.nextLine()).length() > 0) {
            Arrays.stream(line.split(" "))
                    .filter(it -> !it.isEmpty())
                    .forEach(it -> arr.add(Integer.parseInt(it)));
        }

        System.out.print("Sorted: ");
        //Sorting and outputting an array to the screen
        arr.sort(Collections.reverseOrder());
        System.out.println(arr);
    }
}