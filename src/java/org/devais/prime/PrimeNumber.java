package org.devais.prime;

import java.util.Scanner;
import java.util.stream.IntStream;

public class PrimeNumber {

    /**
     * Задание 1
     * Необходимо написать программу, которая вычисляет простые числа в пределах от 1
     * до N. N вводится вручную во время выполнения программы.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number: ");
        int input = scanner.nextInt();

        // loop through the numbers one by one
        for (int i = 2; i < input; i++) {
            boolean isPrimeNumber = true;

            // check to see if the number is prime
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrimeNumber = false;
                    break; // exit the inner for loop
                }
            }
            // print the number if prime
            if (isPrimeNumber) {
                System.out.println(i);
            }
        }
    }
}