package org.devais.palindrom;

import java.util.Scanner;

public class Palindrom {

    /**
     * Задание 5
     * Необходимо написать программу, которая проверяет слово на "палиндромность".
     * Слово для проверки вводится вручную во время выполнения программы.
     */

    public static void main(String[] args) {
        System.out.println("Enter the word: ");
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        isPalindrome(s);
    }

    //revers word
    private static String reverseString(String s) {
        StringBuilder r = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; --i)
            r.append(s.charAt(i));
        return r.toString();
    }

    private static void isPalindrome(String s) {
        if (s.equalsIgnoreCase(reverseString(s))) {
            System.out.println("It's word palindrom");
        } else {
            System.out.println("It's word not palindrom");
        }
    }
}