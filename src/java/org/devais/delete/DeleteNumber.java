package org.devais.delete;

import java.util.Scanner;

public class DeleteNumber {
    /**
     * Задание 6
     * Необходимо написать программу, которая удаляет из текста числа. Текст вводится
     * вручную во время выполнения программы.
     * Результат работы программ выводить на экран.
     */
    public static void main(String[] args) {
        System.out.println("Enter your text: ");
        Scanner sc = new Scanner(System.in);
        String self = sc.nextLine();
        System.out.print(delNum(self));
    }

    private static String delNum(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i)))
                sb.append(s.charAt(i));
        }
        return sb.toString();
    }
}