package org.devais.logical;

import java.util.Arrays;
import java.util.Scanner;

public class LogicDataType {

    /**
     * Задание 4
     * Необходимо написать программу, считывающую четыре логических значения и
     * печатающую “True” в том случае, если ровно два из них истинны. Значения вводятся
     * вручную во время выполнения программы.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter four values ");

        long count = Arrays.stream(sc.nextLine().split(" "))
                .filter(it -> !it.isEmpty())
                .filter(it -> it.toLowerCase().equals("true"))
                .count();

        if (count == 2) {
            System.out.println("True");
        }
    }
}

